import java.awt.event.ActionListener;
import java.awt.image.ConvolveOp;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.security.auth.Refreshable;
import javax.swing.JButton;
import javax.swing.table.TableModel;
import javax.xml.bind.ParseConversionEvent;

import org.hibernate.HibernateException;

import DAO.DAOCompteRendu;
import DAO.DAOVisiteur;
import views.NewCRV;
import views.compteRenduVisite;
import views.connexion;
import model.CompteRenduVisite;
import util.HibernateUtil;

import java.awt.Color;
import java.awt.event.ActionEvent;

public class ControllerCRV implements ActionListener{
	
	private compteRenduVisite fenetre;
	private NewCRV newCRV;
	
	DAOCompteRendu daoCRV;
	List<CompteRenduVisite> listCRV;
	
	MyDefaultModelCompteRendu model;
	
	
	
	public ControllerCRV(compteRenduVisite fenetre, DAOCompteRendu daocrv) {
		
		this.fenetre = fenetre;
		this.daoCRV = daocrv;
		fenetre.getBtn_delete().addActionListener(this);
		fenetre.getBtn_creerCRD_1().addActionListener(this);
					
	}

	public void Init()  {
		
		
		fenetre.setVisible(true);
		listCRV = daoCRV.findAll();
		
		model = new MyDefaultModelCompteRendu(listCRV);
		fenetre.getTable().setModel(model);
		

	}
	
	
	private void delete() throws SQLException {
		int id = fenetre.getTable().getSelectedRow();
		if(id != (-1)) {	
			CompteRenduVisite c = listCRV.get(id);
			listCRV.remove(id);
			daoCRV.delete(c);
			model.fireTableDataChanged();
		}
	}
	
	
	
	private void NewCRV() throws HibernateException, SQLException {
		newCRV.setVisible(true);
		fenetre.setVisible(false);
	//	new ControllerNewCRV(new NewCRV(), new DAOVisiteur(HibernateUtil.getSessionFactory().openSession()));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton buton = (JButton) e.getSource();
		String name = buton.getName();
		switch (name) {
		case "newCRV":
			try {
				NewCRV();
			} catch (HibernateException | SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		case "delete":
			try {
				delete();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;

		default:
			break;
		}
		
		
	}
}

