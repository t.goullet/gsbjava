package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the MEDECIN database table.
 * 
 */
@Entity
@Table(name="MEDECIN")
@NamedQuery(name="Medecin.findAll", query="SELECT m FROM Medecin m")
public class Medecin implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MEDECINId")
	private int medecinId;
	
	@Column(name="MedecinAdresse")
	private String medecinAdresse;

	@Column(name="MedecinMail")
	private String medecinMail;

	@Column(name="MedecinNom")
	private String medecinNom;

	@Column(name="MedecinPassword")
	private String medecinPassword;

	@Column(name="MedecinSpecialite")
	private String medecinSpecialite;


	public Medecin() {
	}

	public String getMedecinAdresse() {
		return this.medecinAdresse;
	}

	public void setMedecinAdresse(String medecinAdresse) {
		this.medecinAdresse = medecinAdresse;
	}

	public String getMedecinMail() {
		return this.medecinMail;
	}

	public void setMedecinMail(String medecinMail) {
		this.medecinMail = medecinMail;
	}

	public String getMedecinNom() {
		return this.medecinNom;
	}

	public void setMedecinNom(String medecinNom) {
		this.medecinNom = medecinNom;
	}

	public String getMedecinPassword() {
		return this.medecinPassword;
	}

	public void setMedecinPassword(String medecinPassword) {
		this.medecinPassword = medecinPassword;
	}

	public String getMedecinSpecialite() {
		return this.medecinSpecialite;
	}

	public void setMedecinSpecialite(String medecinSpecialite) {
		this.medecinSpecialite = medecinSpecialite;
	}



}