package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the PRODUIT database table.
 * 
 */
@Entity
@Table(name="PRODUIT")
@NamedQuery(name="Produit.findAll", query="SELECT p FROM Produit p")
public class Produit implements Serializable {
	private static final long serialVersionUID = 1L;

	public Produit(int produitId, String produitDescription, String produitNom,
			List<CompteRenduVisite> compteRenduVisites, Categorie categorie) {
		super();
		this.produitId = produitId;
		this.produitDescription = produitDescription;
		this.produitNom = produitNom;
		this.compteRenduVisites = compteRenduVisites;
		this.categorie = categorie;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ProduitId")
	private int produitId;
	
	@Column(name="ProduitDescription")
	private String produitDescription;

	@Column(name="ProduitNom")
	private String produitNom;

	//bi-directional many-to-many association to CompteRenduVisite
	@ManyToMany(mappedBy="produits")
	private List<CompteRenduVisite> compteRenduVisites;

	//bi-directional many-to-one association to Categorie
	@ManyToOne
	@JoinColumn(name="ProduitCategorie", referencedColumnName="CategorieId")
	private Categorie categorie;

	public int getProduitId() {
		return produitId;
	}

	public void setProduitId(int produitId) {
		this.produitId = produitId;
	}

	public Produit() {
	}

	public String getProduitDescription() {
		return this.produitDescription;
	}

	public void setProduitDescription(String produitDescription) {
		this.produitDescription = produitDescription;
	}

	public String getProduitNom() {
		return this.produitNom;
	}

	public void setProduitNom(String produitNom) {
		this.produitNom = produitNom;
	}

	public List<CompteRenduVisite> getCompteRenduVisites() {
		return this.compteRenduVisites;
	}

	public void setCompteRenduVisites(List<CompteRenduVisite> compteRenduVisites) {
		this.compteRenduVisites = compteRenduVisites;
	}

	public Categorie getCategorie() {
		return this.categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

}