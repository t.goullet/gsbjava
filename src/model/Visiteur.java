package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the VISITEUR database table.
 * 
 */
@Entity
@Table(name="VISITEUR")
@NamedQuery(name="Visiteur.findAll", query="SELECT v FROM Visiteur v")
public class Visiteur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "VISITEURId")
	private int visiteurId;
	
	@Column(name="VisiteurMail")
	private String visiteurMail;

	@Column(name="VisiteurNom")
	private String visiteurNom;

	@Column(name="VisiteurPassword")
	private String visiteurPassword;

	@Column(name="VisiteurPrenom")
	private String visiteurPrenom;

	
	public Visiteur() {
	}

	public String getVisiteurMail() {
		return this.visiteurMail;
	}

	public void setVisiteurMail(String visiteurMail) {
		this.visiteurMail = visiteurMail;
	}

	public String getVisiteurNom() {
		return this.visiteurNom;
	}

	public void setVisiteurNom(String visiteurNom) {
		this.visiteurNom = visiteurNom;
	}

	public String getVisiteurPassword() {
		return this.visiteurPassword;
	}

	public void setVisiteurPassword(String visiteurPassword) {
		this.visiteurPassword = visiteurPassword;
	}

	public String getVisiteurPrenom() {
		return this.visiteurPrenom;
	}

	public void setVisiteurPrenom(String visiteurPrenom) {
		this.visiteurPrenom = visiteurPrenom;
	}



}