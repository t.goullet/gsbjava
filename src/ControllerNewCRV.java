import java.awt.event.ActionListener;
import java.awt.image.ConvolveOp;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.security.auth.Refreshable;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.ParseConversionEvent;

import org.hibernate.HibernateException;

import DAO.DAOCategorie;
import DAO.DAOCompteRendu;
import DAO.DAOMedecin;
import DAO.DAOProduit;
import DAO.DAOVisiteur;

import views.compteRenduVisite;
import views.NewCRV;
import model.Categorie;
import model.CompteRenduVisite;
import model.Medecin;
import model.Produit;
import model.Visiteur;
import util.HibernateUtil;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.event.ActionEvent;

public class ControllerNewCRV implements ActionListener{
	
	private NewCRV fenetre;
	
	DAOCategorie daoCat;
	DAOProduit daoProd;
	DAOMedecin daoMed;
	DAOVisiteur daoVisi;
	DAOCompteRendu daoCRV;
	List<Produit> listProd;
	
	String[] columns = {
            "Produits"
        };
	
	
	public ControllerNewCRV(NewCRV fenetre, DAOProduit DaoProd, DAOCategorie DaoCat,DAOMedecin daoMed,DAOVisiteur daoVisi, DAOCompteRendu daoCRV) {
		
		this.fenetre = fenetre;
		this.daoCat = DaoCat;
		this.daoProd = DaoProd;
		this.daoMed = daoMed;
		this.daoVisi = daoVisi;
		this.daoCRV = daoCRV;
		fenetre.getBtnRetour().addActionListener(this);
		fenetre.getBtnValider().addActionListener(this);

	}

	public void Init() throws SQLException {
		List<Categorie> med = daoCat.findAll();
		
		for (Categorie categorie : med) {
			fenetre.getComboBox_cat().addItem(categorie.getCategorieNom());
		}
		
				
		
		listProd= daoProd.findAll();
		System.out.println("ok");
	    
		fenetre.setVisible(true);

	}
	

	
	
	
	public void valider() throws HibernateException, SQLException {
		CompteRenduVisite cdr = new CompteRenduVisite();
		cdr.setCRVDate(fenetre.getTxt_date().getText());
		cdr.setCRVHeure(fenetre.getTxt_heure().getText());
		cdr.setCRVDescription(fenetre.getTxt_desc().getText());
		
	
		
		cdr.setMedecin(Medecin);
		cdr.setVisiteur(Visiteur);
		cdr.setProduits(listProd);
		
		daoCRV.saveOrUpdate(cdr);
		
		fenetre.setVisible(false);
		new ControllerCRV(new compteRenduVisite(), new DAOCompteRendu(HibernateUtil.getSessionFactory().openSession())).Init();
			
	}
	
	public void retour() throws HibernateException, SQLException {
		fenetre.setVisible(false);
		new ControllerCRV(new compteRenduVisite(), new DAOCompteRendu(HibernateUtil.getSessionFactory().openSession())).Init();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton buton = (JButton) e.getSource();
		String name = buton.getName();
		
		switch (name) {
			case "valider":
			try {
				valider();
			} catch (HibernateException | SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			break;
		case "retour":
			try {
				retour();
			} catch (HibernateException | SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		default:
			break;
		}		
	}
	
	
	
}