import java.awt.event.ActionListener;
import java.awt.image.ConvolveOp;
import java.sql.SQLException;
import java.util.List;

import javax.security.auth.Refreshable;
import javax.swing.JButton;
import javax.xml.bind.ParseConversionEvent;

import org.hibernate.HibernateException;

import DAO.DAOCompteRendu;
import DAO.DAOGeneric;
import DAO.DAOMedecin;
import DAO.DAOVisiteur;
import views.compteRenduVisite;
import views.connexion;
import model.Medecin;
import model.Visiteur;
import util.HibernateUtil;

import java.awt.Color;
import java.awt.event.ActionEvent;

public class Controller implements ActionListener{
	
	private connexion fenetre;
	DAOVisiteur daoVisiteur;
	List<Visiteur> visiteurs;
	
	DAOMedecin daoMedecin;
	List<Medecin> medecins;
	
	public Controller(connexion fenetre, DAOVisiteur daoVisiteur, DAOMedecin daoMedecin) {
		
		this.fenetre = fenetre;
		this.daoVisiteur = daoVisiteur;
		this.daoMedecin = daoMedecin;
		fenetre.getBtnConnexion().addActionListener(this);
					
	}

	public void Init() {	
		
		visiteurs = daoVisiteur.findAll();
		
		fenetre.setVisible(true);
		
		
	}
	
	private void ConnexionAction() throws HibernateException, SQLException {
		String mail = fenetre.getTextField_user().getText();
		String mdp = fenetre.getTextField_mdp().getText();
		
		if(mail.equals("")) {
			System.out.println("Veuillez entrer un email");
		}
		else {
			Visiteur bdd_visiteur = daoVisiteur.VerificationMailV(mail);	
			if(bdd_visiteur != null) {
				String bdd_mdp = bdd_visiteur.getVisiteurPassword();
				
				if(bdd_mdp.equals(mdp)) {
					new ControllerCRV(new compteRenduVisite(), new DAOCompteRendu(HibernateUtil.getSessionFactory().openSession())).Init();
					fenetre.setVisible(false);
				}else {
					System.out.println("Mot de passe incorrecte");
				}
			}
			
		else {
			Medecin bdd_medecin = daoMedecin.VerificationMailM(mail);	
			if(bdd_medecin != null) {
				String bdd_mdp = bdd_medecin.getMedecinPassword();
					
				if(bdd_mdp.equals(mdp)) {
					new ControllerCRV(new compteRenduVisite(), new DAOCompteRendu(HibernateUtil.getSessionFactory().openSession())).Init();
					fenetre.setVisible(false);
									
					}else {
						System.out.println("Mot de passe incorrecte");
					}
				}
					else {
						System.out.println("Email incorrecte");
					}
				
		}
	}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton buton = (JButton) e.getSource();
		String name = buton.getName();
		switch (name) {
		case "Connexion":
			try {
				ConnexionAction();
			} catch (HibernateException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			break;

		default:
			break;
		}
		
		
	}
}

