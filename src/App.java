import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import DAO.DAOGeneric;
import DAO.DAOMedecin;
import DAO.DAOVisiteur;
import model.Visiteur;
import util.HibernateUtil;
import util.TestConnection;
import views.connexion;

public class App {

	public static void username(int id) {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		Visiteur user = (Visiteur) session.get(Visiteur.class, id);
		System.out.println("Visiteur name : " + user.getVisiteurNom());
	}
	public static void main(String[] args) throws HibernateException, SQLException {
		//System.out.println("Running");
		//TestConnection t = new TestConnection();
		//t.init();
		
		Controller Acceuil = new Controller(new connexion(), new DAOVisiteur(HibernateUtil.getSessionFactory().openSession()), new DAOMedecin(HibernateUtil.getSessionFactory().openSession()));
		Acceuil.Init();
	}
}
