package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.Checkbox;
import java.awt.Button;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class NewCRV extends JFrame {

	private JPanel contentPane;
	private JTextField txt_date;
	private JTextField txt_heure;
	private JTextField txt_desc;
	private JButton btnRetour;
	private JButton btnValider;
	private JComboBox comboBox_cat;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewCRV frame = new NewCRV();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewCRV() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 775, 501);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNouvelleFiche = new JLabel("Nouveau compte rendu de visite");
		lblNouvelleFiche.setBounds(205, 12, 252, 15);
		contentPane.add(lblNouvelleFiche);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(40, 58, 70, 15);
		contentPane.add(lblDate);
		
		JLabel lblHeure = new JLabel("Heure");
		lblHeure.setBounds(190, 58, 70, 15);
		contentPane.add(lblHeure);
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(351, 58, 106, 15);
		contentPane.add(lblDescription);
		
		JLabel lblDatevisiteur = new JLabel("Visiteur");
		lblDatevisiteur.setBounds(547, 58, 70, 15);
		contentPane.add(lblDatevisiteur);
		
		btnRetour = new JButton("Retour");
		btnRetour.setName("retour");
		btnRetour.setBounds(12, 430, 117, 25);
		contentPane.add(btnRetour);
		
		btnValider = new JButton("Valider");
		btnValider.setName("valider");
		btnValider.setBounds(636, 430, 117, 25);
		contentPane.add(btnValider);
		
		txt_date = new JTextField();
		txt_date.setBounds(40, 85, 114, 19);
		contentPane.add(txt_date);
		txt_date.setColumns(10);
		
		txt_heure = new JTextField();
		txt_heure.setColumns(10);
		txt_heure.setBounds(192, 85, 114, 19);
		contentPane.add(txt_heure);
		
		txt_desc = new JTextField();
		txt_desc.setColumns(10);
		txt_desc.setBounds(343, 85, 114, 19);
		contentPane.add(txt_desc);
		
		JLabel lblDatemedicament = new JLabel("Medicament");
		lblDatemedicament.setBounds(343, 134, 114, 15);
		contentPane.add(lblDatemedicament);
		
		JLabel lblCategorie = new JLabel("Categorie");
		lblCategorie.setBounds(40, 134, 114, 15);
		contentPane.add(lblCategorie);
		
		comboBox_cat = new JComboBox();
		comboBox_cat.setBounds(40, 161, 149, 24);
		contentPane.add(comboBox_cat);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(246, 161, 371, 289);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"New column", "New column"
			}
		));
		scrollPane.setViewportView(table);
		
		JComboBox comboBox_cat_2 = new JComboBox();
		comboBox_cat_2.setBounds(547, 82, 114, 24);
		contentPane.add(comboBox_cat_2);
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}
	public JButton getBtnValider() {
		return btnValider;
	}
	public JTextField getTxt_date() {
		return txt_date;
	}
	public JTextField getTxt_heure() {
		return txt_heure;
	}
	public JTextField getTxt_desc() {
		return txt_desc;
	}
	public JComboBox getComboBox_cat() {
		return comboBox_cat;
	}

	public JTable getTable() {
		return table;
	}
}
