package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

public class compteRenduVisite extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btn_delete;
	private JButton btn_creerCRD_1;
	private JButton btnRDV;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					compteRenduVisite frame = new compteRenduVisite();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public compteRenduVisite() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 770, 486);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblComptesRendusDe = new JLabel("Comptes rendus des visites");
		lblComptesRendusDe.setBounds(286, 43, 227, 15);
		contentPane.add(lblComptesRendusDe);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 90, 725, 276);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Medecin", "Visiteur", "Description", "Medicaments", "Date", "Heure", 
			}
		));
		scrollPane.setViewportView(table);
		
		btn_creerCRD_1 = new JButton("+");
		btn_creerCRD_1.setBounds(628, 38, 109, 25);
		contentPane.add(btn_creerCRD_1);
		
		btn_delete = new JButton("Supprimer");
		btn_delete.setName("delete");
		btn_delete.setBounds(286, 415, 209, 25);
		contentPane.add(btn_delete);
		
		btnRDV = new JButton("Prendre un RDV");
		btnRDV.setBounds(23, 33, 164, 25);
		contentPane.add(btnRDV);
	}

	public JButton getBtn_delete() {
		return btn_delete;
	}
	public JButton getBtn_creerCRD_1() {
		return btn_creerCRD_1;
	}
	public JTable getTable() {
		return table;
	}
	public JButton getBtnRDV() {
		return btnRDV;
	}
}
