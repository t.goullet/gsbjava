package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class NouveauRDV extends JFrame {

	private JPanel contentPane;
	private JTextField txt_date;
	private JTextField txt_heure;
	private JButton btnRetour;
	private JButton btnValider;
	private JComboBox comboBox_med;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NouveauRDV frame = new NouveauRDV();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NouveauRDV() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 544, 423);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAjouterUnCrneaux = new JLabel("Prendre un nouveau rendez-vous");
		lblAjouterUnCrneaux.setBounds(38, 32, 310, 15);
		contentPane.add(lblAjouterUnCrneaux);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(54, 120, 70, 15);
		contentPane.add(lblDate);
		
		JLabel lblHheure = new JLabel("Heure");
		lblHheure.setBounds(54, 168, 70, 15);
		contentPane.add(lblHheure);
		
		JLabel lblMedecin = new JLabel("Medecin");
		lblMedecin.setBounds(54, 245, 70, 15);
		contentPane.add(lblMedecin);
		
		btnRetour = new JButton("Retour");
		btnRetour.setName("retour");
		btnRetour.setBounds(12, 331, 117, 25);
		contentPane.add(btnRetour);
		
		btnValider = new JButton("Valider");
		btnValider.setName("valider");
		btnValider.setBounds(391, 331, 117, 25);
		contentPane.add(btnValider);
		
		txt_date = new JTextField();
		txt_date.setBounds(194, 118, 114, 19);
		contentPane.add(txt_date);
		txt_date.setColumns(10);
		
		txt_heure = new JTextField();
		txt_heure.setColumns(10);
		txt_heure.setBounds(194, 166, 114, 19);
		contentPane.add(txt_heure);
		
		comboBox_med = new JComboBox();
		comboBox_med.setBounds(191, 240, 117, 24);
		contentPane.add(comboBox_med);
		
		JLabel lblMedecin_1 = new JLabel("Motif");
		lblMedecin_1.setBounds(54, 206, 70, 15);
		contentPane.add(lblMedecin_1);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(194, 197, 289, 31);
		contentPane.add(textField);
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}
	public JButton getBtnValider() {
		return btnValider;
	}
	public JTextField getTxt_date() {
		return txt_date;
	}
	public JTextField getTxt_heure() {
		return txt_heure;
	}
	public JComboBox getComboBox_med() {
		return comboBox_med;
	}
}
