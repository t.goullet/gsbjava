package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.mapping.Value;

import com.mysql.cj.Query;


public abstract class DAOGeneric <T> {
	
	protected Session session;
	protected Class<T> entityClas;
	
	
	public DAOGeneric(Session session, Class<T> entityClas) {
		this.session = session;
		this.entityClas = entityClas;
	}
	
	public T find(int id){
		T entity = (T) session.get(entityClas,id);
		return entity;
		
	}
	
	public void saveOrUpdate(T entity) {
		
		session.getTransaction().begin();
		session.saveOrUpdate(entity);
		session.getTransaction().commit();
		
	}
	
	public void delete(T entity) {
		
		session.getTransaction().begin();
		session.delete(entity);
		session.getTransaction().commit();
		
	}
	
	public List<T> findAll() {
		
		List<T> value = session.createQuery("FROM " + this.entityClas.getName()).list();
		return  value;
		
	}
	
	public int count() {
		
		List<T> value = session.createQuery("FROM " + this.entityClas.getName()).list();
		return  value.size();
		
		
	}



}
