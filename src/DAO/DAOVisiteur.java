package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


import model.Visiteur;

public class DAOVisiteur extends DAOGeneric<Visiteur>{

	

	public DAOVisiteur(Session session) {
		super(session, Visiteur.class);
		// TODO Auto-generated constructor stub
	}
	
	public Visiteur VerificationMailV(String mail) {
		String SQL = "SELECT * FROM VISITEUR WHERE VisiteurMail=:mail";
		SQLQuery query = session.createSQLQuery(SQL);
		query.setString("mail", mail);
		query.addEntity(entityClas);
		Visiteur  email = (Visiteur ) query.uniqueResult();
		return email;
			
	}
	
	
}

